<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $fillable = [
        'name', 'firstname', 'birthdate'
    ];
    //lien entre 2 tables
    public function has_directed()
    {
        return $this->hasMany('App\Models\Movie');
    }

    public function has_played()
    {
        return $this->belongsToMany('App\Models\Movie')->withPivot('role_name');
    }
}
