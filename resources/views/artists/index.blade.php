<table class="table table-stpied table-centered">
  <thead>
    <tr>
      <th>{{__('Name')}}</th>
      <th>{{__('First Name')}}</th>
      <th>{{__('Actions')}}</th>
    </tr>
  </thead>
  <tbody>
    @foreach($artists as $artist)
    <tr>
      <td>{{ $artist->name }}</td>
      <td>{{ $artist->firstname }}</td>
      <td>
        <a type="button" 
          href="{{ route('artist.edit', $artist->id )}}" 
          class="btn"
          data-toggle="tooltip"
          title="@lang('Edit artist') {{ $artist->name }}">
          Edit
        </a>
      </td>
    </tr>
    @endforeach

  </tbody>
</table>
  <!--pagination pour changer de page-->
  {{ $artists->appends(request()->except('page'))->links() }}
