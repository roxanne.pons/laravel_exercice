@extends('layouts.app')

@section('title', 'Artist Name')

@section('content')
<form method="POST" action="{{ route('artist.update', $artist->id) }}">
  {{ csrf_field() }}
  {{ method_field('PUT') }}
  <p>
    <label for="name">Name</label>
    <input type="text"
      name="name"
      id="name"
      value="{{ $artist->name}}" 
      required
    />
    <label for="firstname">First Name</label>
    <input 
      type="text"
      name="firstname"
      id="firstname"
      value="{{ $artist->firstname}}" 
      required
    />
  </p>
  <button type="submit">
    Create
  </button>
</form>
@endsection