@extends('layouts.app')

@section('title', 'Artist Name')

@section('content')
<form method="POST" action="{{ route('artist.store') }}">
  {{ csrf_field() }}
  <p>
    <label for="name">Name</label>
    <input type="text" name="name" id="name" value="" required />
    <label for="firstname">First Name</label>
    <input type="text" name="firstname" id="firstname" value="" required />
  </p>
  <button type="submit">Create</button>
</form>
@endsection